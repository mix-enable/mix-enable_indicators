This project is used for the MIX-ENABLE project (CORE Organic Cofund Call 2016/17). 

**MIX-ENABLE**: MIXEd livestock farming for improved sustaiNABiLity and robustnEss of organic livestock

# Config
Works with: R version 4.2.2 (2022-10-31 ucrt) -- "Innocent and Trusting"

# Info on dir and files
This project is structured as a Rproject (MIX-ENABLE.Rproj), openable with RStudio.

- data*:
    - sample-30.csv = shuffled and sampled data => NOT REAL, as a reproductible example only. 
    - Real data can be found at: https://data.inrae.fr/dataset.xhtml?persistentId=doi:10.15454/AKEO5G
- scripts/*: all scripts
- references/*: references used in the scripts
A directory "results" is created when the scripts are ran

# Use
- Open the Rproj (`MIX-ENABLE.Rproj`) with Rstudio
- Check the setup file `setup.R`: 
    - mostly: 
        - type of calculation for livestock units you want (default is classic depending on metabolic weight, the other possibility is with a correction according to dairy production)
        - name of the data file (real vs sample) (default is sample)
    - also: choice of directories (results, etc.)
- Run `compute-indicators.R` file