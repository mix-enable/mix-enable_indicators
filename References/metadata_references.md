# Abreviations
SHEEV	share human edible energy value
SHEPV	share human edible proteine value 

Effluents: straw-bedded pen (SBP), straw-bedded cubicles (SBC), straw bedding and scraped alley (SBSA), scraped cubicles (SC) and slatted cubicle (SlC)

# Files
- `References_product_simplified.csv` is an extraction of `References_product.csv` where every product is detailed by country